const NwBuilder = require('nw-builder')
const currentPath = require('path').dirname(require.main.filename)

let nb = new NwBuilder({
  files: `${currentPath}/dist/**/**`, // use the glob format
  buildDir: 'nw-build',
  platforms: ['win32', 'win64', 'osx64', 'linux32', 'linux64']
})

// Log stuff you want
nb.on('log', console.log)

nb.build()
  .then(function () {
    console.log('all done!')
  })
  .catch(function (error) {
    console.error(error)
  })
