import ZonaApi from '../Connectors/Zona'

export default class Search {
  constructor () {
    this.SearchProvider = new ZonaApi()
    this.reset()
  }

  setString (string) {
    this.searchString = ''
  }

  setPage (page) {
    this.page = page
  }

  includeGenres (genres) {
    this.genres = []
    genres.forEach((genre) => {
      this.genres.push(genre.id)
    })
  }

  excludeGenres (genres) {
    this.excludedGenres = []
    genres.forEach((genre) => {
      this.excludedGenres.push(genre.id)
    })
  }

  reset () {
    this.page = 0
    this.searchString = ''
    this.pageSize = 60
    this.genres = []
    this.excludedGenres = []
  }

  search () {
    return new Promise((resolve, reject) => {
      this.SearchProvider.search(
        {
          start: this.page * this.pageSize,
          rows: this.pageSize,
          notGenreId: this.excludedGenres,
          genreId: this.genres,
          query: this.searchString
        },
        (result) => {
          resolve(result)
        }
      )
    })
  }

  getGenres () {
    return new Promise((resolve, reject) => {
      this.SearchProvider.getGenreId(function (response) {
        let result = []
        response.response.docs.forEach((doc) => {
          result.push({
            id: doc.id,
            title: doc.name
          })
        })
        resolve(result)
      })
    })
  }
}
