import { Search } from './Search'

export class SearchPlugin {
  constructor () {
    this.search = new Search();
  }

  install (Vue, options) {
    this.Vue = Vue
    this.options = options
    this.searchString = ''
    Vue.prototype.$search = this.search
  }
}
