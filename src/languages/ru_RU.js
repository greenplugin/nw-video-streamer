export default {
  __info: {
    title: 'Russian',
    code: 'ru_RU',
    localizedTitle: 'Русский'
  },
  common: {
    phraseWelcomeToStreamer: 'Добро пожаловать в Air Streamer',
    phrasePleaseInsertUrl: 'Пожалуйста вставьте URL',
    wordURL: 'URL',
    wordFile: 'Файл',
    wordYoutube: 'Youtube',
    wordTorrent: 'Торрент',
    wordDevices: 'Устройства',
    __InDevelopment: 'В разработке'
  },
  header: {},
  settings: {
    title: 'Настройки Air Streamer.',
    headerCommon: 'Общие',
    headerTorrent: 'Торрент',
    headerStorage: 'Хранилище',
    phraseSettingsCommon: 'Общие настройки',
    phraseSettingsStorage: 'Настройки хранилища',
    phraseSettingsTorrent: 'Настройки торрент клиента',
    phraseSelectPathForLibrary: 'Выберите путь для библиотеки',
    phraseSelectPathForDownloads: 'Выберите путь для загрузок',
    phraseSelectLanguage: 'Выбрите язык',
    phraseAllowSeeding: 'Разрешить сидирование после загрузки',
    phraseAutoStart: 'Разрешить автоматический запуск загрузки'
  },
  torrent: {},
  system: {}
}
