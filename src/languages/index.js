import ruRU from './ru_RU'
import enUS from './en_US'

let locales = [
  ruRU,
  enUS
]
let lang = {}

init(locales)

function init (locales) {
  locales.forEach(locale => {
    lang[locale.__info.code] = locale
  })
}

let keys = Object.keys(lang)

export { lang as default, keys, lang }
