export default {
  __info: {
    title: 'English',
    code: 'en_US',
    localizedTitle: 'English'
  },
  common: {
    phraseWelcomeToStreamer: 'Velcome to Air Streamer',
    phrasePleaseInsertUrl: 'Please insert the URL',
    wordURL: 'URL',
    wordFile: 'File',
    wordYoutube: 'Youtube',
    wordTorrent: 'Torrent',
    wordDevices: 'Devices',
    __InDevelopment: 'In Development'
  },
  header: {},
  settings: {
    title: 'Air Streamer Setup.',
    headerCommon: 'Common',
    headerTorrent: 'Torrent',
    headerStorage: 'Storage',
    phraseSettingsCommon: 'Common settings',
    phraseSettingsStorage: 'Storage settings',
    phraseSettingsTorrent: 'Torrent settings',
    phraseSelectPathForLibrary: 'Select path to library',
    phraseSelectPathForDownloads: 'Select path to downloads',
    phraseSelectLanguage: 'Select language',
    phraseAllowSeeding: 'Allow seeding after downloading',
    phraseAutoStart: 'Allow auto start download'
  },
  torrent: {},
  system: {}
}
