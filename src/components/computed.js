import bus from '../bus.js'
import events from '../events.js'

export default {
  url: {
    get () {
      return this.$store.state.player.url
    },
    set (value) {
      this.$store.commit('player.url', value)
      bus.$emit(events.player.url, value)
    }
  },
  selectedDevice: {
    get () {
      return this.$store.state.player.selectedDevice
    },
    set (value) {
      this.$store.commit('player.selectedDevice', value)
    }
  }
}
