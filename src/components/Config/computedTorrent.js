export default {
  torrentAllowSeeding: {
    get () {
      return this.$store.state.settings.torrent.allowSeeding
    },
    set (value) {
      this.$store.commit('settings/torrent.allowSeeding', value)
    }
  },
  torrentAutoStart: {
    get () {
      return this.$store.state.settings.torrent.autoStart
    },
    set (value) {
      this.$store.commit('settings/torrent.autoStart', value)
    }
  }
}
