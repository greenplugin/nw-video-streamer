export default {
  commonLang: {
    get () {
      return this.$store.state.settings.common.lang
    },
    set (value) {
      this.$store.commit('settings/common.lang', value)
    }
  }
}
