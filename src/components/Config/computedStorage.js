export default {
  storageLibraryPath: {
    get () {
      return this.$store.state.settings.storage.libraryPath
    },
    set (value) {
      this.$store.commit('settings/storage.libraryPath', value)
    }
  },
  storageDownloadsPathPath: {
    get () {
      return this.$store.state.settings.storage.downloadsPath
    },
    set (value) {
      this.$store.commit('settings/storage.downloadsPath', value)
    }
  }
}
