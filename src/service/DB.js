import Datastore from 'nedb'

class DB {
  constructor (path) {
    this.ready = false
    this.db = new Datastore({filename: path})
  }

  init () {
    return new Promise((resolve, reject) => {
      this.db.loadDatabase((err) => {
        if (err) throw err
        this.ready = true
        resolve(true)
      })
    })
  }

  insert (document) {
    return new Promise((resolve) => {
      this.db.insert(document, (err, newDoc) => {
        if (err) throw err
        resolve(newDoc._id)
      })
    })
  }

  find (query) {
    return new Promise((resolve) => {
      this.db.find(query, (err, docs) => {
        if (err) throw err
        resolve(docs)
      })
    })
  }

  query (query, sort = null, start = null, length = null) {
    return new Promise((resolve) => {
      let q = this.db.find(query)
      if (sort !== null) {
        q.sort(sort)
      }
      if (start !== null) {
        q.skip(start)
      }
      if (length !== null) {
        q.limit(length)
      }
      q.exec((err, docs) => {
        if (err) throw err
        resolve(docs)
      })
    })
  }
}

export default DB
