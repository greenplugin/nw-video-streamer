import storage from './localStorage/Storage'
import configCommon from './config/common.js'
import configTorrent from './config/torrent.js'
import configStorage from './config/storage.js'

class Config {
  constructor () {
    this.settings = storage.getLocalStorage('settings')
    this.normalizeSettings()
    this.settings.flush()
  }

  paramConditions () {
    return {
      ...configStorage,
      ...configTorrent,
      ...configCommon
    }
  }

  normalizeSettings () {
    let conditions = this.paramConditions()
    Object.keys(conditions).forEach((path) => {
      this.checkParam(path, conditions[path].default, conditions[path].condition)
    })
  }

  checkParam (path, def, condition) {
    let value = false
    try {
      value = this.settings.get(path)
    } catch (e) {
      condition = (value) => { return false }
    }

    if (!condition(value)) {
      console.warn(`${path} configuration not valid, resetting to default`)
      this.settings.set(path, def)
    }
  }

  get (path) {
    return this.settings.get(path)
  }

  set (path, value) {
    if (!this.paramConditions()[path]) throw new Error(`parameter ${path} not supported`)
    if (!this.paramConditions()[path].condition(value)) throw new Error(`the parameter ${path} must satisfy the condition`)
    this.settings.set(path, value)
    this.settings.flush()
  }
}

export default new Config()
