class LocalStorage {
  /**
   *
   * @returns {string}
   * @param {string} storageName
   */
  constructor (storageName) {
    if (window.storageClass && window.storageClass[storageName]) {
      throw new Error('storage already opened')
    }
    if (window.localStorage) {
      this.localStorage = window.localStorage
    } else {
      this.localStorage = {
        getItem () {
          return '{}'
        },
        setItem () {
          console.warn('Storage not available, cannot save data to storage, page reloading will erase all data')
        }
      }
    }
    this.name = storageName
    let value = this.localStorage.getItem(storageName)
    if (value === null) {
      value = '{}'
      this.localStorage.setItem(storageName, value)
    }
    this.storage = JSON.parse(value)
  }

  /**
   *
   * @param path
   * @param value
   * @returns {LocalStorage}
   */
  set (path, value) {
    if (!path || !path.length) {
      throw new Error('Path can not be empty')
    }

    let paths = path.split('.')
    let result = this.storage

    paths.forEach((item, iteration) => {
      if (!item) {
        throw new Error('Path can not be empty')
      }
      if (!result[item]) {
        result[item] = {}
      }
      if (iteration === paths.length - 1) {
        result[item] = value
      } else {
        result = result[item]
      }
    })

    return this
  }

  /**
   *
   * @param path
   * @returns {LocalStorage}
   */
  delete (path) {
    if (!path || !path.length) {
      throw new Error('Path can not be empty')
    }

    let paths = path.split('.')
    let result = this.storage

    paths.forEach((item, iteration) => {
      if (!item) {
        throw new Error('Path entry can not be empty')
      }
      if (!result[item]) {
        throw new Error('item not found')
      }
      if (iteration === paths.length - 1) {
        delete result[item]
      } else {
        result = result[item]
      }
    })

    return this
  }

  /**
   *
   * @param path
   * @returns {object | *}
   */
  get (path) {
    if (!path || path === null) {
      return this.storage
    }
    let paths = path.split('.')
    let result = this.storage

    paths.forEach((item) => {
      if (!item || !item.length) {
        throw new Error('Path entry can not be empty')
      }
      if (!(result instanceof Object)) {
        throw new Error(`Item '${item}' not found in path '${path}'`)
      }

      result = result[item]
    })

    return result
  }

  /**
   *
   * @returns {string[]}
   */
  getKeys () {
    return Object.keys(this.storage)
  }

  /**
   *
   * @returns {LocalStorage}
   */
  flush () {
    this.localStorage.setItem(this.name, JSON.stringify(this.storage))
    return this
  }
}

export default LocalStorage
