import LocalStorage from './LocalStorage'
import CacheStorage from './CacheStorage'

class Storage {
  /**
   * @param storageName
   * @returns LocalStorage
   */
  static getLocalStorage (storageName = '_nwStorage') {
    if (!window.storageClass) window.storageClass = {}

    if (!window.storageClass[storageName]) {
      window.storageClass[storageName] = new LocalStorage(storageName)
    }
    return window.storageClass[storageName]
  }

  /**
   *
   * @param name
   * @returns CacheStorage
   */
  static getCache (name = '_nwCache') {
    if (!window.cacheClass) window.cacheClass = {}

    if (!window.cacheClass[name]) {
      window.cacheClass[name] = new CacheStorage(Storage.getLocalStorage(name))
    }

    return window.cacheClass[name]
  }

  static flushAll () {
    window.storageClass.foreach((storage) => {
      storage.flush()
    })
  }
}

export default Storage
