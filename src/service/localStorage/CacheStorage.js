/**
 * @property {LocalStorage} localStorage
 */
class CacheStorage {
  /**
   *
   * @param localStorage
   */
  constructor (localStorage) {
    this.localStorage = localStorage
  }

  /**
   *
   * @param name
   * @param includeExpired
   * @returns {CacheItem | boolean}
   */
  get (name, includeExpired = false) {
    let item = false
    try {
      item = new CacheItem(this.localStorage.get(name))
    } catch (e) {
      console.warn(e)
      return false
    }
    return item.isNotExpired() || includeExpired ? item : false
  }

  /**
   *
   * @param name
   * @param includeExpired
   * @returns {CacheItem | boolean}
   */
  getValue (name, includeExpired = false) {
    let item = this.get(name, includeExpired)
    return item ? item.value : false
  }

  /**
   *
   * @param item
   * @param name
   * @param lifeTime
   * @returns {CacheStorage}
   */
  set (item, name, lifeTime = 1200) {
    // TODO [GreenPlugin]: use moment for parsing interval
    let newItem = new CacheItem(item, lifeTime)
    this.localStorage.set(name, newItem.getData())
    return this
  }

  /**
   *
   * @returns {CacheStorage}
   */
  flush (lazy = false) {
    if (!lazy) {
      this.localStorage.getKeys().forEach(key => {
        let item = this.get(key, true)
        if (item && item.isExpired()) {
          this.localStorage.delete(key)
        }
      })
    }
    this.localStorage.flush()
    return this
  }
}

class CacheItem {
  /**
   *
   * @param value
   * @param expires
   */
  constructor (value, expires = false) {
    if (expires) {
      let created = Math.round(+new Date() / 1000)
      this.value = value
      this.lifeTime = expires
      this.created = created
    }

    if (value instanceof Object && !expires) {
      if (!value.v && !value.l && !value.c) {
        throw new Error('Value is not valid CacheItem data')
      }
      this.value = value.v
      this.lifeTime = value.l
      this.created = value.c
    }
  }

  /**
   *
   * @param value
   * @returns {CacheItem}
   */
  update (value) {
    let created = Math.round(+new Date() / 1000)
    this.value = value
    this.created = created
    return this
  }

  /**
   *
   * @param seconds
   * @returns {CacheItem}
   */
  prolong (seconds) {
    this.expires += seconds
    return this
  }

  /**
   *
   * @returns {boolean}
   */
  isExpired () {
    let now = Math.round(+new Date() / 1000)
    return this.created + this.lifeTime <= now
  }

  /**
   *
   * @returns {boolean}
   */
  isNotExpired () {
    return !this.isExpired()
  }

  getData () {
    return {
      v: this.value,
      l: this.lifeTime,
      c: this.created
    }
  }
}

export default CacheStorage
