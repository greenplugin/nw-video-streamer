import shortId from 'shortid'

class Device {
  constructor (device, type) {
    this.shortId = shortId.generate()
    this.device = device
    this.type = type
    this.info = {}
  }

  play (url) {
    return this[`${this.type}Play`](url)
  }

  resume () {
    this[`${this.type}Resume`]()
  }

  pause () {
    this[`${this.type}Pause`]()
  }

  stop () {
    this[`${this.type}Stop`]()
  }

  getName () {
    return this[`${this.type}Name`]()
  }

  getTitle () {
    return this.getName()
  }

  forward (seconds) {
    return this[`${this.type}Forward`](seconds)
  }

  backward (seconds) {
    return this[`${this.type}Backward`](seconds)
  }

  getPlaybackPosition () {
    return this[`${this.type}PlaybackPosition`]()
  }

  getStatus () {
    return this[`${this.type}Status`]()
  }

  navigate (positionInSeconds) {
    return this[`${this.type}Navigate`](positionInSeconds)
  }

  airplayPlay (url) {
    return new Promise((resolve, reject) => {
      this.device.play(url, (err) => {
        if (err) reject(err)
        resolve('play')
      })
    })
  }

  airplayPause () {
    this.device.pause()
  }

  airplayResume () {
    this.device.resume()
  }

  airplayStop () {
    this.device.stop()
  }

  airplayStatus () {
    return new Promise((resolve, reject) => {
      this.device.playbackInfo((err, e, a) => {
        if (err) reject(err)
        resolve(a)
      })
    })
  }

  airplayGetScrub () {
    return new Promise((resolve, reject) => {
      this.device.scrub((err, e, a) => {
        if (err) throw err
        resolve(a)
      })
    })
  }

  airplaySetScrub (seconds) {
    return new Promise((resolve, reject) => {
      this.device.scrub(seconds, (err, e, a) => {
        if (err) throw err
        resolve(a)
      })
    })
  }

  airplayPlaybackDuration (status) {
    if (!status) {
      return status.duration
    }
    return new Promise(resolve => {
      this.airplayStatus()
        .then(status => {
          resolve(status.duration)
        })
    })
  }

  airplayPlaybackPosition (status) {
    if (status) {
      return status.position
    }
    return new Promise(resolve => {
      this.airplayStatus().then(status => {
        resolve(status.position)
      })
    })
  }

  airplayForward (seconds) {
    this.airplayStatus().then(status => {
      let position = this.airplayPlaybackPosition(status)
      this.airplaySetScrub(position + seconds)
    })
  }

  airplayNavigate (positionInSeconds) {
    console.info(positionInSeconds)
    this.airplaySetScrub(positionInSeconds)
  }

  airplayBackward (seconds) {
    this.airplayForward(-seconds)
  }

  airplayName () {
    return this.device.name.split('@')[0]
  }
}

export default Device
