import airplaer from 'airplayer'
import Device from './Device'

class Devices {
  constructor () {
    this.devices = {}

    this.onUpdate = () => {}

    try {
      let airplayList = airplaer()
      airplayList.on('update', (device) => {
        console.info(device)
        this.pushDevice('airplay', device)
      })
      airplayList.on('error', (e) => {
        console.error(e)
      })
    } catch (e) {
      window.console.error(e)
    }

    setInterval(() => {
      this.locateDevices()
    }, 100)
  }

  locateDevices () {

  }

  pushDevice (proto, device) {
    let dev = new Device(device, proto)
    this.devices[dev.getName()] = dev
    this.fireOnUpdate(dev)
  }

  fireOnUpdate (device) {
    this.onUpdate(this.devices, device)
  }

  addUpdateListener (fn) {
    this.onUpdate = fn
  }

  removeDevice (device) {
    delete this.devices[device.getName()]
  }
}

function createDevicesWatcher () {
  let devices = window.devices
  if (!devices) {
    window.devices = devices = new Devices()
  }
  return devices
}

export default createDevicesWatcher()
