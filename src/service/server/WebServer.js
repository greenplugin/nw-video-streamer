import http from 'http'
import fs from 'fs'
import mimeTypes from 'mime-types'
import shortId from 'shortid'
import network from 'network'
import rangeParser from 'range-parser'

class WebServer {
  constructor () {
    this.path = false
    this.server = this.createServer()
    this.statusListeners = []
    this.state = {
      listening: false,
      fileSelected: false
    }
  }

  createServer () {
    return http.createServer((request, response) => {
      console.info(request)
      if (!this.path) {
        response.writeHead(200, {'Content-Type': 'plain/text'})
        response.end('Nw Video Streamer is running, please select file', 'utf-8')
      } else {
        let stream
        const contentType = mimeTypes.lookup(this.path)
        response.statusCode = 200
        response.setHeader('Content-Type', contentType)
        response.setHeader('Accept-Ranges', 'bytes')
        let range = rangeParser(this.fileLength, request.headers.range || '')
        if (Array.isArray(range)) {
          response.statusCode = 206 // indicates that range-request was understood
          range = range[0]
          response.setHeader(
            'Content-Range',
            'bytes ' + range.start + '-' + range.end + '/' + this.fileLength
          )
          response.setHeader('Content-Length', range.end - range.start + 1)
          stream = fs.createReadStream(this.path, {start: range.start, end: range.end})
        } else {
          response.setHeader('Content-Length', this.fileLength)
          console.info(this.path)
          stream = fs.createReadStream(this.path)
        }
        stream.pipe(response)
      }
    })
  }

  setStreamingPath (p = false) {
    return new Promise((resolve, reject) => {
      this.url = shortId.generate()
      this.path = p
      fs.stat(this.path, (err, stats) => {
        if (err) {
          throw err
        }
        this.fileLength = stats.size
      })
      network.get_private_ip((err, ip) => {
        if (err) throw err
        this.ip = (ip || '127.0.0.1')
        this.state.listening = this.server.listening
        this.state.fileSelected = !!this.path
        this.status()
        resolve(this.getUrl())
      })
    })
  }

  start (port) {
    this.port = port
    return new Promise((resolve, reject) => {
      this.url = shortId.generate()
      network.get_private_ip((err, ip) => {
        if (err) throw err
        this.ip = (ip || '127.0.0.1')
      })
      this.server.listen(port, () => {
        this.state.listening = this.server.listening
        this.state.fileSelected = !!this.path
        this.status()
        resolve(this.server)
      })
      this.server.on('error', (err) => {
        this.state.listening = this.server.listening
        this.state.fileSelected = !!this.path
        this.status()
        reject(err)
      })
    })
  }

  stop () {
    return new Promise((resolve, reject) => {
      this.server.close((status) => {
        this.state.listening = this.server.listening
        this.state.fileSelected = !!this.path
        this.status()
        resolve(status)
      })
    })
  }

  onChangeStatus (fn) {
    this.statusListeners.push(fn)
  }

  status () {
    this.statusListeners.forEach((fn) => {
      fn(this.state)
    })
    return this.state
  }

  getUrl () {
    return `http://${this.ip}:${this.port}/${this.url}`
  }
}

export default WebServer
