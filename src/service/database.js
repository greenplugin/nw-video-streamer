import DB from './DB.js'
import fs from 'fs'
import path from 'path'
import config from './config.js'

class Database {
  constructor () {
    this.databases = {}
    this.libraryPath = `${config.get('storage.libraryPath')}/db`
    this.constructor.mkDirSync(this.libraryPath)
  }

  /**
   *
   * @param name
   * @returns {Promise<DB>}
   */
  get (name) {
    return new Promise(resolve => {
      if (this.databases[name]) {
        resolve(this.databases[name])
      } else {
        let filename = name.replace(/\./g, '_')
        this.databases[name] = new DB(`${this.libraryPath}/${filename}.nedb`)
        this.databases[name].init().then(result => {
          resolve(this.databases[name])
        })
      }
    })
  }

  static mkDirSync (dirPath) {
    const parts = dirPath.split(path.sep)
    for (let i = 1; i <= parts.length; i++) {
      let p = path.join.apply(null, parts.slice(0, i))
      if (!fs.existsSync(p)) {
        fs.mkdirSync(p)
      }
    }
  }
}

export default new Database()
