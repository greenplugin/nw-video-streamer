import { keys } from '../../languages/index'
import osLocale from 'os-locale'

function getOsLocale () {
  let locale = osLocale.sync()
  return keys.includes(locale) ? locale : keys[0]
}

export default {
  'common.lang': {
    condition: (value) => {
      return keys.includes(value)
    },
    default: getOsLocale()
  }
}
