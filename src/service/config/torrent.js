export default {
  'torrent.allowSeeding': {
    condition: (value) => {
      return (typeof (value) === 'boolean')
    },
    default: true
  },
  'torrent.autoStart': {
    condition: (value) => {
      return (typeof (value) === 'boolean')
    },
    default: true
  }
}
