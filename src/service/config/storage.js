import fs from 'fs'
import nwGui from 'nw.gui'

export default {
  'storage.libraryPath': {
    condition (value) {
      if (!value) return false
      if (!(typeof (value) === 'string')) return false
      if (!value.length) return false
      try {
        return fs.lstatSync(value).isDirectory()
      } catch (e) {
        return false
      }
    },
    default: `${nwGui.App.dataPath}/library/`
  },
  'storage.downloadsPath': {
    condition (value) {
      if (!value) return false
      if (!(typeof (value) === 'string')) return false
      if (!value.length) return false
      try {
        return fs.lstatSync(value).isDirectory()
      } catch (e) {
        return false
      }
    },
    default: 'tmp/'
  }
}
