export default {
  player: {
    url: 'player.url',
    play: 'player.play',
    stop: 'player.stop',
    pause: 'player.pause',
    navigate: 'player.navigate',
    forward: 'player.forward',
    backward: 'player.backward',
    actions: {
      play: 'play',
      stop: 'stop',
      pause: 'pause',
      backward: 'backward',
      forward: 'forward'
    }
  }
}
