import Vue from 'vue'
import Router from 'vue-router'
import HomeScreen from '@/components/HomeScreen'
import ConfigScreen from '@/components/ConfigScreen'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HomeScreen',
      component: HomeScreen
    },
    {
      path: '/options',
      name: 'ConfigScreen',
      component: ConfigScreen
    }
  ]
})
