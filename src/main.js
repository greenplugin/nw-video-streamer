// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import nw from 'nw.gui'

import Vue from 'vue'
import Vuex from 'vuex'
import vuexI18n from 'vuex-i18n'
import App from './App'
import router from './router'
import config from './service/config.js'
import UIkit from 'uikit'
import Icons from 'uikit/dist/js/uikit-icons'
import customIcons from './assets/icons.js'
import store from './Storage'
import moment from 'moment'

import { lang, keys as langKeys } from './languages/index.js'

if (process.env.NODE_ENV === 'development') {
  nw.Window.get().showDevTools()
}

Vue.use(Vuex)

Vue.use(vuexI18n.plugin, store)

// Vue.i18n.add('en', languages.en)
langKeys.forEach((key) => {
  Vue.i18n.add(key, lang[key])
})

moment.locale(config.get('common.lang'))
Vue.i18n.set(config.get('common.lang'))

UIkit.use(Icons)

UIkit.icon.add(customIcons)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: {App}
})
