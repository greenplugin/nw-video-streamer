import config from '../service/config.js'
import UiKit from 'uikit'

function setConfig (path, value) {
  try {
    config.set(path, value)
  } catch (e) {
    UiKit.notification({
      message: e.message,
      status: 'danger',
      pos: 'top-right',
      timeout: 5000
    })
  }
}

export default {
  namespaced: true,
  state: {
    common: {
      lang: config.get('common.lang')
    },
    storage: {
      libraryPath: config.get('storage.libraryPath'),
      downloadsPath: config.get('storage.downloadsPath')
    },
    torrent: {
      allowSeeding: config.get('torrent.allowSeeding'),
      autoStart: config.get('torrent.autoStart')
    }
  },
  mutations: {
    'storage.libraryPath' (state, value) {
      setConfig('storage.libraryPath', value)
      state.storage.libraryPath = value
    },
    'storage.downloadsPath' (state, value) {
      setConfig('storage.downloadsPath', value)
      state.storage.downloadsPath = value
    },
    'torrent.allowSeeding' (state, value) {
      setConfig('torrent.allowSeeding', value)
      state.torrent.allowSeeding = value
    },
    'torrent.autoStart' (state, value) {
      setConfig('torrent.autoStart', value)
      state.torrent.autoStart = value
    },
    'common.lang' (state, value) {
      setConfig('common.lang', value)
      state.common.lang = value
    }
  }
}
