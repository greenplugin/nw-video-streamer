import Vue from 'vue'
import Vuex from 'vuex'
import settings from './settings.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    settings: settings
  },
  state: {
    app: {
      title: 'Welcome to awesome torrent player'
    },
    player: {
      selectedDevice: false,
      url: ''
    }
  },
  mutations: {
    'app:title' (state, newTitle) {
      state.app.title = newTitle
    },
    'player.selectedDevice' (state, device) {
      state.player.selectedDevice = device
    },
    'player.url' (state, url) {
      state.player.url = url
    }
  }
})
