/**
 * Created by nikolai on 13.07.17.
 */
let request = require('request')

function ZonaApi (console) {
  this.console = window.console
}

ZonaApi.prototype = {

  search: function (options, callback) {
    let This = this

    let notGenreId = ''
    let genreId = ''
    let searchString = ''

    if (!options.start) { options.start = 0 }
    if (!options.rows) { options.rows = 60 }

    if (options.genreId) {
      if (options.genreId.length) {
        let res1 = options.genreId.join('+AND+')
        genreId = `AND(genreId:(${res1}))`
      }
    }

    if (options.query) {
      options.query = encodeURIComponent(options.query)
      searchString = `AND((name_rus:"${options.query}")OR(name_original:"${options.query}"))`
    }

    if (options.notGenreId) {
      if (options.notGenreId.length) {
        let res2 = options.notGenreId.join('+OR+')
        notGenreId = `NOT(genreId:(${res2}))` // NOT(genreId:(12+OR+15+OR+25+OR+26+OR+1747+OR+28+OR+27+OR+tv))
      }
    }

    let url = `http://zsolr2.zonasearch.com/solr/movie/select/?q=(NOT(abuse:zona)((languages:en)OR((subtitles:en)AND(languages:null)))AND(adult:false)AND(tor_count:[1+TO+2147483647])AND(indexed:[1+TO+7])AND(serial:false)${genreId}AND(quality:[1+TO+7]+AND+(audio_quality:[1+TO+3]+OR+audio_quality:15))${notGenreId}${searchString})&version=2.2&wt=json&sort=popularity%20desc,seeds%20desc,id%20desc&fl=id,year,playable,trailer,quality,audio_quality,type3d,serial,languages_imdb,rating,genre,runtime,episodes,tor_count,serial_end_year,serial_ended,abuse,release_date_int,release_date_rus,indexed,geo_rules,partner_entity_id,partner_type,name_rus,name_ukr,name_eng,name_original&start=${options.start}&rows=${options.rows}`

    this.console.log({u: url})// .replace(/\s/g, '%20'));
    request({url: url},
// eslint-disable-next-line handle-callback-err
      function (error, response, body) {
        let result = JSON.parse(body)
        This.console.info(result)

        callback(result)
      })
  },

  getInfo: function (movieId, callback, context = null) {
    request({url: 'http://zsolr2.zonasearch.com/solr/movie/select/?q=((id:' + movieId + ')AND(adult:false))&version=2.2&wt=json&start=0&rows=1'},
// eslint-disable-next-line handle-callback-err
      function (error, response, body) {
        let result = JSON.parse(body)

        result = result.response.docs[0]

        result.genre_name = result.genre_name.split(', ')
        result.country = result.country.split(', ')

        result.persons = JSON.parse(result.persons)

        callback(result, context)
      })
  },

  imagesGet: function (movieId) {
    let urlHost = 'http://img3.zonapic.com/images/film_240/'
    movieId = movieId.toString()

    let sl = ''

    switch (movieId.length) {
      case 7:
        sl = movieId.slice(0, 4)
        break
      case 6:
        sl = movieId.slice(0, 3)
        break
      case 5:
        sl = movieId.slice(0, 2)
        break
      case 4:
        sl = movieId.slice(0, 1)
        break
      case 3:
        sl = 0
        break
      default:
    }

    return urlHost + sl + '/' + movieId + '.jpg'
  },

  getBackdroImg: function (backdropId) {
    let urlHost = 'http://img4.zonapic.com/images/backdrop_1280/'
    backdropId = backdropId.toString()

    let sl = ''

    switch (backdropId.length) {
      case 7:
        sl = backdropId.slice(0, 4)
        break
      case 6:
        sl = backdropId.slice(0, 3)
        break
      case 5:
        sl = backdropId.slice(0, 2)
        break
      case 4:
        sl = backdropId.slice(0, 1)
        break
      case 3:
        sl = 0
        break
      default:
    }

    return urlHost + sl + '/' + backdropId + '.jpg'
  },

  getActorImg: function (actorId) {
    let urlHost = 'http://img2.zonapic.com/images/actor/'
    actorId = actorId.toString()

    let sl = ''

    switch (actorId.length) {
      case 7:
        sl = actorId.slice(0, 4)
        break
      case 6:
        sl = actorId.slice(0, 3)
        break
      case 5:
        sl = actorId.slice(0, 2)
        break
      case 4:
        sl = actorId.slice(0, 1)
        break
      case 3:
        sl = 0
        break
      default:
    }

    return urlHost + sl + '/' + actorId + '.jpg'
  },

  getTorrentList: function (movieId, callback, context = null) {
    request({url: 'http://zsolr2.zonasearch.com/solr/torrent/select/?q=(kinopoisk_id:' + movieId + ')AND(indexed:[1+TO+7])&fl=id,kinopoisk_id,broadcast_id,translate_info,files,episodes,episodesInfo,episodes_map_auto,languages,language,languages_mod,languages_parser,subtitles,subtitles_mod,subtitles_parser,private,hash,torrent_download_link,seeds,peers,size_bytes,type3d,quality_id,audio_quality_id,trailer,resolution,video_info,loading_time_sum,loading_success_count,loading_fail_count&version=2.2&start=0&rows=2147483647&wt=json'},
// eslint-disable-next-line handle-callback-err
      function (error, response, body) {
        let result = JSON.parse(body)

        result = result.response.docs

        result.sort(function (a, b) {
          return parseInt(b.seeds, 10) - parseInt(a.seeds, 10)
        })

        result.forEach(function (file) {
          file.files = JSON.parse(file.files)
        })

        callback(result, context)
      })
  },

  getGenreId: function (callback) {
    request({url: 'http://zsolr.zonasearch.com/solr/genre2/select/?q=*:*&version=2.2&wt=json&fl=id,name,ord,adult,fictional,custom&start=0&rows=2147483647'},
// eslint-disable-next-line handle-callback-err
      function (error, response, body) {
        let result = JSON.parse(body)

        result.response.docs.sort(function (a, b) {
          return parseInt(a.ord, 10) - parseInt(b.ord, 10)
        })

        callback(result)
      })
  }

}

module.exports = ZonaApi
