
function ZonaUserApi() {

}

ZonaUserApi.prototype = {

    remindPassword: function (username, callback) {
        request({url: 'http://sync.zonasearch.com/user/remindPassword?username=' + username + '&locale=ru'},
            function (error, response, body) {
                let result = JSON.parse(body);
                callback(result);
            });
        //
        //{
        //    data: "remindSendOk"
        //}
    },

    login: function (username, password, callback) {
        request({url: 'http://sync.zonasearch.com/user/checkPassword?username=' + username + '&password=' + password},
            function (error, response, body) {
                let result = JSON.parse(body);
                callback(result);
            });
    },

    reg: function (username, password, callback) {
        request({url: 'http://sync.zonasearch.com/user/registration?username=' + username + '&password=' + password + '&locale=ru'},
            function (error, response, body) {
                let result = JSON.parse(body);
                callback(result);
            });
    },

};

module.exports = ZonaUserApi;