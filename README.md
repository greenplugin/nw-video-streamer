# nw video streamer

> the app for streaming torrent video

## Build Setup

``` bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# build nw package
npm run build-nw

```

./cache - cache files of nw builder

./dist - compiled files

./nw-build - builded native apps for ['win32', 'win64', 'osx64', 'linux32', 'linux64']
